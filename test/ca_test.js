const CertificateAuthority = artifacts.require("CertificateAuthority");

contract("CertificateAuthority", accounts => {
    it("should create a new credential", () => {
        const issuer = accounts[0];
        const TestContext = {
            credentialHolder: accounts[1],
            attributes: {
                name: "Jon Sno",
                issuer,
                issueDate: Date.now().toString,
                validUntil: new Date().setFullYear(new Date().getFullYear() + 1),
                vaccinatedFor: "measles"
            }
        }
        const credentialAttributes = JSON.stringify(TestContext.attributes);

        CertificateAuthority.deployed()
        .then(instance => instance.NewCredential(
            TestContext.credentialHolder,
            credentialAttributes, 
            { from: issuer }
            )
        )
        .then(res => {
            assert.equal(res.receipt.status, true)
        })
        .catch(e => {
            console.error(e);
        })
    });
});