pragma solidity >=0.4.25;

/// @title CertificateAuthority
/// @author Oluwatomisin (Tomisin) Jenrola
/// @notice this contract demonstrates the creation of a credential by an issuer
contract CertificateAuthority {
    
    /// @notice this structure is agnostic of the credential type
    /// @param id is the unique identifier of the credential
    /// @param holder is the credential holder
    /// @param attributes desciribe what the credential is about
    struct Credential {
        uint id;
        address holder;
        string attributes;
    }
    
    /// @notice the authority assigning certificates
    address public issuer;
    
    /// @notice the list of all credentials issued
    /// @param the address of a credential holder
    mapping(address => Credential[]) public credentials;
    
    /// @notice this event notifies the network about the new CredentialCreated
    /// @param credentialHolder the credential holder
    event CredentialCreated(address credentialHolder);
    
    modifier onlyIssuer {
        _;
        require(msg.sender == issuer);
    }
    
    /// @notice this assigns the contract deployer as the issuer of credentials
    constructor() public {
        issuer = msg.sender;
    }
    
    /// @notice stores a credential on the ledger and emits an event
    /// @param _holder is the credential holder
    /// @param _attributes is the data about the credential
    /// @return the index of the new credential
    function NewCredential(address _holder, string memory _attributes) onlyIssuer public returns (uint) {
        require(_holder != address(0));
        require(bytes(_attributes).length != 0);
        
        uint index = credentials[_holder].length;
        Credential memory cred = Credential(index, _holder, _attributes);
        credentials[_holder].push(cred);

        emit CredentialCreated(_holder);
        return index;
    }
    
}